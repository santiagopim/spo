(defpackage #:pool-address
  (:use #:common-lisp)
  (:export #:match-address))

(in-package #:pool-address)

;;; To reduce SSD disk wear out, it is recomended to use this script in a RAM disk, because
;;; the cardano-cli keys generator writes three files every time:
;;;
;;; $ sudo mkdir /tmp/poolramdisk
;;; $ sudo chmod 700 /tmp/poolramdisk
;;; $ sudo chown santiago:santiago /tmp/poolramdisk
;;; $ sudo mount -t tmpfs -o size=1m poolramdisk /tmp/poolramdisk
;;; $ cd /tmp/poolramdisk/

(uiop:chdir "/tmp/poolramdisk/")

(defun gen-keys ()
  (uiop:run-program
   "cardano-cli node key-gen --verification-key-file node.vkey --cold-signing-key-file node.skey --operational-certificate-issue-counter-file node.counter"))

(defun get-pid ()
  (uiop:run-program
   "cardano-cli stake-pool id --cold-verification-key-file node.vkey" :output :string))

(defun match-address (wished-word)
  (gen-keys)
  (let ((pid (get-pid))
        (end-point (+ 5 (length wished-word))))
    (do ((iter 1 (1+ iter)))
        ((string= (subseq pid 5 end-point) wished-word)
         (format t "~&*** DONE in ~A: ~A" iter pid)
         (finish-output))
      (gen-keys)
      (setq pid (get-pid)))))

;;; Without using the filesystem, it is possible to get the pool id from a 64 bytes hex
;;; string that uses as vkey, and then write out the counter file from there.
;;; TODO: BUT THE SKEY !?

(defun get-hex ()
  "Get an array of 32 unsigned byte 8, hence from 0 to 255, and transform to
an array of hexadecimal."
  (do ((i 31 (- i 1))
       (bytes (make-array 32 :element-type '(unsigned-byte 8))))
      ((< i 0)
       (crypto:byte-array-to-hex-string bytes))
    (setf (aref bytes i) (random 256))))

(defun get-pid-from-hex (h)
  "Get the stake pool id in bech32 format from the hexadecimal parameter."
  (uiop:run-program
   (concatenate 'string "cardano-cli stake-pool id --stake-pool-verification-key " h)
   :output :string))

(defun match-address-ram (wished-word)
  "Iterate the pool stake id generation up to match the first letters with the
wished word parameter. Pool stake addresses have the 'pool1' sufix, so the check
is done with the first characters from the 5 one:

  pool1----n0nh5svzph2kjt0vxawl2uptskuc75ffcgte6k4dc4dnt6d
       ^^^^
       wished word
"
  (do* ((iteration 1 (1+ iteration))
        (hex (get-hex) (get-hex))
        (pid (get-pid-from-hex hex) (get-pid-from-hex hex))
        (end-point (+ 5 (length wished-word)))
        (wished-word (sb-unicode:lowercase wished-word)))
       ((string= (subseq pid 5 end-point) wished-word)
        (format t "~&*** DONE in ~A: ~A" iteration pid)
        (format t "~&*** With chain: ~A" hex)
        (finish-output)
        (write-poolid pid)
        (write-vkey hex)
        (write-counter hex))))

;;; Write results on disk:

(defun write-poolid (pid)
  (with-open-file (out "poolid.bech32"
                       :direction :output 
                       :if-exists :supersede
                       :if-does-not-exist :create)
    (format out pid)))

(defun write-vkey (hex)
  (with-open-file (out "cold.vkey"
                       :direction :output 
                       :if-exists :supersede
                       :if-does-not-exist :create)
    (format out (concatenate 'string
                             "{
    \"type\": \"StakePoolVerificationKey_ed25519\",
    \"description\": \"Stake Pool Operator Verification Key\",
    \"cborHex\": \"5820" hex "\"
}
"))))

(defun write-counter (hex)
  (with-open-file (out "cold.counter"
                       :direction :output 
                       :if-exists :supersede
                       :if-does-not-exist :create)
    (format out (concatenate 'string
                             "{
    \"type\": \"NodeOperationalCertificateIssueCounter\",
    \"description\": \"Next certificate issue number: 0\",
    \"cborHex\": \"82005820" hex "\"
}
"))))
