{-# LANGUAGE OverloadedStrings #-}
{-# LANGUAGE OverloadedStrings #-}
{-# LANGUAGE OverloadedStrings #-}

module Main where

import Turtle 
import Data.Typeable 

{- |
To reduce SSD disk wear out, it is recomended to use this script in a RAM disk, because
the cardano-cli keys generator writes three files every time:

$ sudo mkdir /tmp/poolramdisk
$ sudo chmod 700 /tmp/poolramdisk
$ sudo chown santiago:santiago /tmp/poolramdisk
$ sudo mount -t tmpfs -o size=1m poolramdisk /tmp/poolramdisk
$ cd /tmp/poolramdisk/
-}

-- genkeys = proc "cardano-cli"
--                ["node", "key-gen"
--                , "--verification-key-file", "node.vkey"
--                , "--cold-signing-key-file", "node.skey"
--                , "--operational-certificate-issue-counter-file", "node.counter"]
--                empty
    
-- getpid = do
--   pid <- shell "cardano-cli stake-pool id --cold-verification-key-file node.vkey" empty
--   return pid

main = do
  cd "/tmp/poolramdisk"
  proc "cardano-cli"
               ["node", "key-gen"
               , "--verification-key-file", "node.vkey"
               , "--cold-signing-key-file", "node.skey"
               , "--operational-certificate-issue-counter-file", "node.counter"]
               empty
  pid <- shell "cardano-cli stake-pool id --cold-verification-key-file node.vkey" empty
  print (id pid)
--  genkeys
--  thepid <- getpid
--  spid <- drop 5 (take 9 pid)
--  print spid
--  print thepid

